import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        question4();
    }

    static void question1() {
        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                System.out.print("Nhập vào số a: ");
                int a = scanner.nextInt();
                System.out.print("Nhập vào số b: ");
                int b = scanner.nextInt();

                if (b == 0) {
                    throw new ArithmeticException();
                }
                System.out.println("Kết quả a/b = " + ((float) a / b));

                break;//kết thúc vòng lặp while - đồng nghĩa kết thúc chương trình
            } catch (InputMismatchException ex) {
                System.out.println("Số bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
            } catch (ArithmeticException ex) {
                System.out.println("Bạn đang thực hiện phép chia cho 0. Vui lòng kiểm tra lại!");
            }
        }
    }

    static void question1_2() {
        int a, b;
        //Nhập số a đến khi đúng, nếu không yêu cầu nhập lại
        while (true) {
            while (true) {
                try {
                    Scanner scanner = new Scanner(System.in);
                    System.out.print("Nhập vào số a: ");
                    a = scanner.nextInt();
                    break;//kết thúc vòng lặp while nhap so a
                } catch (InputMismatchException ex) {
                    System.out.println("Số a bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
                }
            }

            //Nhập số b đến khi đúng, nếu không yêu cầu nhập lại
            while (true) {
                try {
                    Scanner scanner = new Scanner(System.in);
                    System.out.print("Nhập vào số b: ");
                    b = scanner.nextInt();
                    break;//kết thúc vòng lặp while nhap so b
                } catch (InputMismatchException ex) {
                    System.out.println("Số b bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
                }
            }

            //Xu ly phep chia a/b=?
            try {
                if (b == 0) {
                    throw new ArithmeticException();
                }
                System.out.println("Kết quả a/b = " + ((float) a / b));
                break;
            } catch (ArithmeticException ex) {
                System.out.println("Bạn đang thực hiện phép chia cho 0. Vui lòng kiểm tra lại!");
            }
        }
    }

    static void question1_3() {
        int a = 0, b = 0;
        boolean numberAValid = false;
        while (true) {
            Scanner scanner = new Scanner(System.in);
            if (!numberAValid) {
                try {
                    System.out.print("Nhập vào số a: ");
                    a = scanner.nextInt();
                    numberAValid = true;
                } catch (InputMismatchException ex) {
                    System.out.println("Số a bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
                    continue;
                }
            }

            try {
                System.out.print("Nhập vào số b: ");
                b = scanner.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Số b bạn vừa nhập không đúng định dạng. Vui lòng nhập lại!");
                continue;
            }

            try {
                if (b == 0) {
                    throw new ArithmeticException();
                }
                System.out.println("Kết quả a/b = " + ((float) a / b));

                break;
            } catch (ArithmeticException ex) {
                System.out.println("Bạn đang thực hiện phép chia cho 0. Vui lòng kiểm tra lại!");
            }
        }
    }

    static void question2() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào số a: ");
        int a = scanner.nextInt();
        System.out.print("Nhập vào số b: ");
        int b = scanner.nextInt();

//        System.out.println(checkDivider(a, b) ? "Đây là phép chia hết." : "Đây là phép chia có dư.");

//        boolean status = checkDivider(a, b);
//        if(status) {
//            System.out.println("Đây là phép chia hết.");
//        }else{
//            System.out.println("Đây là phép chia có dư.");
//        }

        try {
            System.out.println(checkDividerOtherWay(a, b) ? "Đây là phép chia hết." : "Đây là phép chia có dư.");
        } catch (ArithmeticException ex) {
            System.out.println("Số b phải khác 0.");
        }
    }

    static boolean checkDivider(int a, int b) {
        try {
            if (b == 0) {
                throw new ArithmeticException();
            }
            return a % b == 0;
        } catch (ArithmeticException ex) {
            System.out.println("Bạn đang thực hiện phép chia cho 0. Vui lòng kiểm tra lại!");
        }
        return false;
    }

    static boolean checkDividerOtherWay(int a, int b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException();
        }
        return a % b == 0;
    }

    static void question3() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào số a: ");
        int a = scanner.nextInt();

        try {
            boolean status = checkDivideBy3(a);
            System.out.println(status ? "Chia hết cho 3." : "Không chia hết cho 3.");
        } catch (MyException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static boolean checkDivideBy3(int a) throws MyException {
        if (a < 0) {
            throw new MyException();
        }
        return a % 3 == 0;
    }

    static void question4() {
        ArrayList<Student> students = new ArrayList<>();

        while (students.size() < 5) {
            System.out.println("Nhập thông tin sinh viên số " + (students.size() + 1));
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập tên sinh viên: ");
            String name = scanner.nextLine();

            scanner = new Scanner(System.in);
            System.out.print("Nhập tên lớp: ");
            String className = scanner.nextLine();

            scanner = new Scanner(System.in);
            System.out.print("Nhập điểm số: ");
            float grade = 0;
            try {
                grade = scanner.nextFloat();
            }catch (InputMismatchException ex) {
                System.out.println("Điểm số không đúng định dạng. Vui lòng nhập lại thông tin của sinh viên này.");
                continue;
            }

            Student student = new Student(name, className, grade);
            students.add(student);
        }

        //Có đủ thông tin 5 sinh viên trong list students

    }
}